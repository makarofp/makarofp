package calculator;

/**
 * Created by makarof on 26.11.2016.
 */
public class Arithmetic {
    // метод для перемножения всех чисел и вывода на экран результата
    public double arrayMultiplication(double[] multiplyArray, int n) {
        double k = 1;
        for (int i = 0; i < multiplyArray.length; i++) {
            k = k * multiplyArray[i];
        }
        System.out.println("Результат умножения всех чисел массива " + k);
        System.out.println();
        System.out.println();
        return k;
    }

    // метод для возведения числа в степень и вывода на экран результата
    public double power(double pow1, double pow2) {
        double p = 0;
        p = Math.pow(pow1, pow2);
        System.out.println("Результат возведения в степень " + p);
        System.out.println();
        System.out.println();
        return p;
    }

    // метод для деления чисел и вывода на экран результата.
    public double division(double del1, double del2) {
        double d = 0;
        d = del1 / del2;
        System.out.println("Результат деления " + d);
        System.out.println();
        System.out.println();
        return d;
    }

    // метод для вычисления квадратного корня из числа и вывода на экран результата.
    public double root(double ch1) {
        double r = 0;
        r = Math.sqrt(ch1);
        System.out.println("Корень квадратный из числа " + ch1 + " равен " + r);
        System.out.println();
        System.out.println();
        return r;
    }
}

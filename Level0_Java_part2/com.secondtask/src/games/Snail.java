package games;

/**
 * Created by makarof on 27.11.2016.
 */
public class Snail {
    // метод вычисляет центральный элемент в массиве и начиная с него заполняет массив цифрами, начиная с 1 по возростанию. Очередность поворотов: вправо, вниз, влево, вверх
    public int[][] calculateSnail(int size) {
        int[][] arSnail = new int[size][size];
        int d = 1;
        int i = (size - 1) / 2;
        int j = (size - 1) / 2;
        int min_i = i;
        int max_i = i;
        int min_j = j;
        int max_j = j;
        for (int a = 1; a <= size * size; a++) {
            arSnail[i][j] = a;
            switch (d) {
                // смещяемся вправо
                case 1:
                    j += 1;
                    if (j > max_j) {
                        d = 2;
                        max_j = j;
                    }
                    break;
                // смещяемся вниз
                case 2:
                    i += 1;
                    if (i > max_i) {
                        d = 3;
                        max_i = i;
                    }
                    break;
                // смещяемся влево
                case 3:
                    j -= 1;
                    if (j < min_j) {
                        d = 4;
                        min_j = j;
                    }
                    break;
                // смещяемся вверх
                case 4:
                    i -= 1;
                    if (i < min_i) {
                        d = 1;
                        min_i = i;
                    }
                    break;
            }
        }
        for (int ii = 0; ii < arSnail.length; ii++) {
            System.out.println();
            for (int jj = 0; jj < arSnail[ii].length; jj++) {
                System.out.print(arSnail[ii][jj] + "\t");
            }
        }
        System.out.println();
        System.out.println();
        return arSnail;
    }
}

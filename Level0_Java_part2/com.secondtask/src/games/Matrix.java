package games;

import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.System.out;

/**
 * Created by makarof on 25.11.2016.
 */

public class Matrix {
    // метод для заполнения матрицы числами от 1 до 9 и вывода на экран результата
    public int[][] getmatrix(int msize) {
        int[][] ar = new int[msize][msize];
        int a = 1;
        for (int i = 0; i < ar.length; i++) {
            for (int j = 0; j < ar[i].length; j++) {
                ar[i][j] = a;
                System.out.print(ar[i][j] + " ");
                if (a < 9) {
                    a++;
                } else a = 1;
            }
            System.out.println();
        }
        return ar;
    }
}

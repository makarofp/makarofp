package games;

/**
 * Created by makarof on 27.11.2016.
 */
public class Palindrom {
    // метод для проверки слова на палиндромизм. Метод сначала уменьшает регистр, а потом проверяет слово на палиндромизм. Результат проверки выводит на экран
    public boolean checkWord(String someWord) {
        String lowerReg = someWord.toLowerCase();
        StringBuffer buffer = new StringBuffer(lowerReg);
        StringBuffer reverseWord = buffer.reverse();
        if (lowerReg.equals(String.valueOf(reverseWord)) == true) {
            System.out.println("Вы ввели слово палиндром");
        } else {
            System.out.println("Cлово не является палиндромом");
        }
        return lowerReg.equals(String.valueOf(reverseWord));
    }

    // метод для проверки фразы на палиндромизм. Метод сначала уменьшает регистр, удаляет пробелы, а потом проверяет фразу на палиндромизм. Результат проверки выводит на экран
    public boolean checkPhrase(String somePhrase) {
        String lowerReg1 = somePhrase.toLowerCase();
        String removeSpace = lowerReg1.replaceAll(" ", "");
        StringBuffer buffer1 = new StringBuffer(removeSpace);
        StringBuffer reversePhrase = buffer1.reverse();
        if (removeSpace.equals(String.valueOf(reversePhrase)) == true) {
            System.out.println("Вы ввели фразу палиндром");
        } else {
            System.out.println("Фраза не является палиндромом");
        }
        return removeSpace.equals(String.valueOf(reversePhrase));
    }
}

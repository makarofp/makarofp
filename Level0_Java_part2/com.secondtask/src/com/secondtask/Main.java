package com.secondtask;

import java.util.Scanner;
import java.io.IOException;

import calculator.Arithmetic;
import games.Matrix;
import games.Palindrom;
import games.Snail;

public class Main {

    public static void main(String[] args) {
        /*Задание 1: ввести размерность квадратной матрицы, и заполнить её цифрами от 1 до 9. Класс Matrix*/

        try {
            int msize;
            do {

                Matrix matrix = new Matrix();
                Scanner sc = new Scanner(System.in);
                System.out.println("Введите размерность матрицы от 1 до 9 и нажмите enter. Для перехода к следующему заданию введите 0:  "); // предлагаем пользователю ввести имя
                msize = sc.nextByte();
                if (msize == 0) {
                    System.out.println("Переходим к следующему заданию");
                    System.out.println();
                    break;
                }
                if (msize > 0 && msize <= 9) {
                    matrix.getmatrix(msize);
                } else {
                    System.out.println("Вы ввели не правильное значение. Давайте еще разок попробуем. ");
                }
            } while (msize != 0);
        } catch (Exception e) {
            System.out.println("Вы ввели некорректное значение. Текст ошибки: " + e);
        }

        /*задание 2: выбрать одну из операций (перемножить элементы в матрице, возвести число в степень, разделить числа,квадратный корень из числа) и ввести для неё значения. Класс Arithmetic*/

        try {
            char choice;
            do {
                Arithmetic arithmetic = new Arithmetic();
                System.out.println("Введите 1, что бы перемножить числа в масиве");
                System.out.println("Введите 2, что бы возвести число в степень");
                System.out.println("Введите 3, что бы разделить числа");
                System.out.println("Введите 4, что бы вычислить квадратный корень");
                System.out.println("Пожалуйста, сделайте свой выбор. Для перехода к следующему заданию введите 0");
                Scanner sc = new Scanner(System.in);
                switch (choice = (char) sc.nextInt()) {
                    case 1:
                        System.out.println("Введите количество элементов массива  ");
                        int n = sc.nextInt();
                        if (n < 1) {
                            System.out.println("Ты что, тестировщик? Заново");
                        } else {
                            double[] multiplyArray = new double[n];
                            System.out.println("Введите элементы массива через enter");
                            for (int i = 0; i < n; i++) {
                                multiplyArray[i] = sc.nextInt(); // Заполняем массив элементами, введёнными с клавиатуры
                            }
                            System.out.println("Введеный Вами массив:");
                            for (int i = 0; i < n; i++) {
                                System.out.print(multiplyArray[i] + "\t"); // Выводим на экран, полученный массив
                            }
                            System.out.println();
                            arithmetic.arrayMultiplication(multiplyArray, n);
                        }
                        break;
                    case 2:
                        System.out.println("Введите число, которое желаете возвести в степень (десятичную дробь вводите через запятую):  ");
                        double pow1 = sc.nextDouble();
                        System.out.println("Введите степень, в которую желаете возвести число (десятичную дробь вводите через запятую):  ");
                        double pow2 = sc.nextDouble();
                        arithmetic.power(pow1, pow2);
                        break;
                    case 3:
                        System.out.println("Введите делимое: ");
                        double del1 = sc.nextDouble();
                        System.out.println("Введите делитель:  ");
                        double del2 = sc.nextDouble();
                        if (del2 == 0) {
                            System.out.println("Делить на ноль нельзя");
                            System.out.println();
                        } else {
                            arithmetic.division(del1, del2);
                        }
                        break;
                    case 4:
                        System.out.println("Введите число, из которого надо вычислить квадратный корень ");
                        double ch1 = sc.nextDouble();
                        if (ch1 < 0) {
                            System.out.println("Чем ты на уроках математики занимался? Квадратные корни из отрицательных чисел извлечь нельзя! Давай повторим ");
                        } else {
                            arithmetic.root(ch1);
                        }
                        break;
                    case 0:
                        System.out.println("Переходим к следующему заданию");
                        System.out.println();
                        break;
                    default:
                        System.out.println("Вы ввели некоректное значение. Будьте внимательнее");
                }
            } while (choice != 0);
        } catch (Exception e) {
            System.out.println("Вы ввели некорректное значение. Текст ошибки: " + e);
        }

        /*Задание 3: улитка. Размерность матрицы вводим с клавиатуры. Заполнить матрицу цифрами начиная с центрального элемента.  Класс Snail*/

        try {
            int size;
            do {
                Snail snail = new Snail();
                Scanner sc = new Scanner(System.in);
                System.out.println("Привет от улитки!!!");
                System.out.println("Введите длину стороны квадрата и улитка заполнить его цифрами (длина может быть равна 3 и более). Для перехода к следующему заданию введите 0.  ");
                size = sc.nextByte();
                if (size == 0) {
                    System.out.println("Переходим к следующему заданию");
                    System.out.println();
                    break;
                }
                if (size > 2) {
                    snail.calculateSnail(size);
                } else {
                    System.out.println("Вы ввели неккоректное значение. Еще раз попробуем.");
                }
            } while (size != 0);
        } catch (Exception e) {
            System.out.println("Вы ввели некорректное значение. Текст ошибки: " + e);
        }

        /*Задание 4: проверить, является ли введённое слово (не учитывая регистр) или фраза (не учитывая регистр и пробелы) полиндромом. Класс Palindrom*/

        try {
            char polichoice;
            do {
                Palindrom palindrom = new Palindrom();
                System.out.println("Вы точно что-то хотите проверить на палиндромизм =)");
                System.out.println("Введите 1, если слово");
                System.out.println("Введите 2, если фразу");
                System.out.println("Пожалуйста, сделайте свой выбор. Для выхода из программы введите 0.");
                Scanner sc = new Scanner(System.in);
                switch (polichoice = (char) sc.nextInt()) {
                    case 1:
                        Scanner sc1 = new Scanner(System.in);
                        System.out.println("Введите cлово");
                        String someWord = sc1.nextLine();
                        palindrom.checkWord(someWord);
                        break;
                    case 2:
                        Scanner sc2 = new Scanner(System.in);
                        System.out.println("Введите фразу");
                        String somePhrase = sc2.nextLine();
                        palindrom.checkPhrase(somePhrase);
                        break;
                    case 0:
                        System.out.println("На сегодня всё=) Пока-пока");
                        break;
                    default:
                        System.out.println("Вы ввели некоректное значение. Давайте еще разок.");
                }
            } while (polichoice != 0);
        } catch (Exception e) {
            System.out.println("Вы ввели некорректное значение. Текст ошибки: " + e);
        }
    }
}
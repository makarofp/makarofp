package com.welcome;

import java.util.Scanner; // импортируем класс

/**
 * Created by makarof on 22.11.2016.
 */
public class Hello {
    private String name;

    public void SetupName() { // создаем метод SetupName с модификатором public
        Scanner sc = new Scanner(System.in); // создаем объект класса Scanner
        System.out.print("Введите имя: "); // предлагаем пользователю ввести имя
        name = sc.nextLine(); // считываем значение с ввода и сохраняемя в переменную типа string
    }

    public void welcome() {  // создаем метод welcome с модификатором public
        System.out.println("Hello, " + name); // выводим на экран
    }

    public void byeBay() { // создаем метод byeBay с модификатором public
        System.out.println("Bye," + name); // выводим на экран
    }
}

package resttestpack;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * Created by makarof on 15.02.2017.
 */
public class TestApilayer {
    String url = "http://apilayer.net/api/live";
    String access_key = "access_key=f208413b43c382be2f83281665d23bc0";
    public static HttpClient client;

    @BeforeClass
    public static void requestCreate() {
        client = HttpClientBuilder.create().build();

    }

    public JSONObject json() throws IOException {
        HttpGet getRequest = new HttpGet(url);
        HttpResponse response = client.execute(getRequest);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
        String result = "", line = "";
        while ((line = rd.readLine()) != null) {
            result += line;
        }
        JSONObject json = new JSONObject(result);
        return json;
    }

    /*Запрос содержит корректный ключ и currencies=EUR. Проверяем, что в ответе есть все необходимые ключи:
    success, terms, privacy, timestamp, source, quotes
    */
    @Test
    public void checkCorrectResponse() throws IOException {
        url += "?" + access_key + "&currencies=EUR";
        JSONObject curObj = json();
        List<String> keyString = Arrays.asList("terms", "success", "privacy", "source", "timestamp", "quotes");
        Assert.assertTrue("В ответе не хватает некоторых ключей. Нам вернуло только: " + curObj.keySet(), curObj.keySet().containsAll(keyString));
    }

    /*Запрос содержит корректный ключ и currencies=EUR,UAH,RUB. Проверить, что quotes содержит только ключи "USDEUR", "USDUAH", "USDRUB" и не содержит друг\ие ключи.
    */
    @Test
    public void checkQuotesContainsOnlyRightParametres() throws IOException {
        url += "?" + access_key + "&currencies=EUR,UAH,RUB";
        JSONObject curObj = json();
        List<String> keyString = Arrays.asList("USDEUR", "USDRUB", "USDUAH");
        Assert.assertTrue("У quotes есть лишние ключи. Вот весть список ключей quotes: "
                + curObj.getJSONObject("quotes").keySet(), curObj.getJSONObject("quotes").length() == 3);
        Assert.assertTrue("Одного из ключей USDEUR, USDRUB, USDUAH нет в quotes. Вот весть список ключей quotes: "
                + curObj.getJSONObject("quotes").keySet(), curObj.getJSONObject("quotes").keySet().containsAll(keyString));
    }

    /*Запрос содержит корректный ключ и параметр currencies без значений. Проверить, что quotes содержит 170 комбинаций валют.
    Тест будет фейлится, так как комбинаций всего 169*/
    @Test
    public void checkQuotesContains170Variants() throws IOException {
        url += "?" + access_key + "&currencies";
        JSONObject curObj = json();
        Assert.assertTrue("У quotes не 170 комбинаций валют. Их: " + curObj.getJSONObject("quotes").keySet().size(),
                curObj.getJSONObject("quotes").keySet().size() == 170);
    }


    /*Запрос содержит корректный ключ и currencies=EUR,UAH. Проверить, что quotes содержит для комбинации "USDEUR" курс, значение которого находится в пределах от 0.9 до 0.99
    */
    @Test
    public void checkRatioUSDEURBetween09and099() throws IOException {
        url += "?" + access_key + "&currencies=EUR,UAH";
        JSONObject curObj = json();
        Assert.assertTrue("Значение курса USDEUR не находится в пределах от 0.9 до 0.99. Он равен: " + curObj.getJSONObject("quotes").get("USDEUR"),
                curObj.getJSONObject("quotes").getDouble("USDEUR") >= 0.9 & curObj.getJSONObject("quotes").getDouble("USDEUR") <= 0.99);
    }

    /*Запрос содержит корректный ключ и source=UAH. Проверить, что в ответе есть error с кодом ошибки 105 и сообщением
    "Access Restricted - Your current Subscription Plan does not support Source Currency Switching."
    */
    @Test
    public void checkError105AndMessageAccessRestricted() throws IOException {
        url += "?" + access_key + "&source=UAH";
        JSONObject curObj = json();
        Assert.assertTrue("Код ошибки не равен 105. Он: " + curObj.getJSONObject("error").getInt("code"),
                curObj.getJSONObject("error").getInt("code") == 105);
        Assert.assertTrue("Не корректное сообщение. Пришло такое: " + curObj.getJSONObject("error").getString("info"),
                curObj.getJSONObject("error").getString("info").equals("Access Restricted - Your current Subscription Plan does not support Source Currency Switching."));
    }
}

package soaptestpack;

import net.yandex.speller.services.spellservice.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by makarof on 13.02.2017.
 */

public class TestSpeller {
    public static SpellService service;
    public static SpellServiceSoap port;
    public static CheckTextRequest request;

    @Before
    public void SpellServiceCreate() {
        service = new SpellService();
        port = service.getSpellServiceSoap();
    }

    @BeforeClass
    public static void NewRequestCreate() {
        request = new CheckTextRequest();
    }

    @Test
    public void testPhraseErrorCountEqualThree() throws IOException {
        request.setLang("EN");
        request.setText("Hillo man, howe are yaou? Phrase with 3 errors");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("Количество ошибок не равно 3. Их: " + checkTextResponse.getSpellResult().getError().size(),
                checkTextResponse.getSpellResult().getError().size() == 3);
    }

    @Test
    public void testFindErrorPosAndRow() throws IOException {
        request.setLang("EN");
        request.setText("Hello man, howe are you? Error: position: 11, row:0");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("Ошибка в позиции: " + checkTextResponse.getSpellResult().getError().get(0).getPos(),
                checkTextResponse.getSpellResult().getError().get(0).getPos() == 11);
        Assert.assertTrue("Ошибка в строке: " + checkTextResponse.getSpellResult().getError().get(0).getRow(),
                checkTextResponse.getSpellResult().getError().get(0).getRow() == 0);
    }

    @Test
    public void testFindErrorPosAndRowInSeveralLinePhrase() throws IOException {
        request.setLang("EN");
        request.setText("Hello man,\nhowe are you? Error: position: 11, row:1");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("Ошибка в позиции: " + checkTextResponse.getSpellResult().getError().get(0).getPos(),
                checkTextResponse.getSpellResult().getError().get(0).getPos() == 11);
        Assert.assertTrue("Ошибка в строке: " + checkTextResponse.getSpellResult().getError().get(0).getRow(),
                checkTextResponse.getSpellResult().getError().get(0).getRow() == 1);
    }

    @Test
    public void testRussianWord() throws IOException {

        request.setLang("RU");
        request.setText("Пьеса была сиграна");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("Не правильное слово СИГРАНА. Вместо него попало: " +
                        checkTextResponse.getSpellResult().getError().get(0).getWord(),
                checkTextResponse.getSpellResult().getError().get(0).getWord().contains("сиграна"));
        Assert.assertTrue("Замена не правильная. Одно из предложенных слов должно быть СЫГРАНА, а предложило: "
                        + checkTextResponse.getSpellResult().getError().get(0).getS(),
                checkTextResponse.getSpellResult().getError().get(0).getS().contains("сыграна"));
    }

    @Test
    public void testQuantityOfSuggestedSubstitutions() throws IOException {
        request.setLang("RU");
        request.setText("сиграна");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("Cлово СИГРАНА не правильное. А программа посчитала его правильным",
                checkTextResponse.getSpellResult().getError().get(0).getWord().contains("сиграна"));
        Assert.assertTrue("Количество ошибок не 5, а: " + checkTextResponse.getSpellResult().getError().get(0).getS().size(),
                checkTextResponse.getSpellResult().getError().get(0).getS().size() == 5);
        Assert.assertTrue("Замена не правильная. Одно из предложенных слов должно быть СЫГРАНА",
                checkTextResponse.getSpellResult().getError().get(0).getS().contains("сыграна"));

    }
}

package com.oop;

import java.awt.*;
import java.util.Random;

import figur.Circle;
import figur.Quadrate;
import figur.Shape;
import figur.Triangle;

public class Main {
    public static Shape[] randshapes = new Shape[10];

    public static void main(String[] args) {

        Random randNumber = new Random();
        int j;
        // заполним массив фигур произвольными фигурами (треугольник, квадрат, круг)
        System.out.println("Массив введёных фигур:");
        for (int i = 0; i < 10; i++) {
            int z = randNumber.nextInt(3);
            switch (z) {
                //добавляем в массив треугольник. Сделаем допущение, что наш треугольник всегда будет равнбедренным и прямоугольным.
                case 0:
                    Triangle triangle = new Triangle(new Point((int) (Math.random() * 10 + 1), (int) (Math.random() * 10 + 1)), Math.floor(Math.random() * 19 + 1), "треугольник");
                    randshapes[i] = triangle;
                    break;
                //добавляем в массив квадрат.
                case 1:
                    Quadrate quadrate = new Quadrate(new Point((int) (Math.random() * 10 + 1), (int) (Math.random() * 10 + 1)), Math.floor(Math.random() * 19 + 1), "квадрат    ");
                    randshapes[i] = quadrate;
                    break;
                case 2:
                    Circle circle = new Circle(new Point((int) (Math.random() * 10 + 1), (int) (Math.random() * 10 + 1)), Math.floor(Math.random() * 19 + 1), "круг       ");
                    randshapes[i] = circle;
                    break;
            }
        }
        // Выведем массив фигур на экран
        for (int i = 0; i < 10; i++) {
            System.out.println();
            System.out.print("Фигура " + randshapes[i].name);
            System.out.print("\t| Координата первой точки: [" + randshapes[i].position.getX() + ", " + randshapes[i].position.getY() + "]");
            System.out.print("\t| Длина стороны/радиуса: ");
            System.out.printf("\t%11.2f", (randshapes[i].side));
        }
        // вызовем метод, который сдвигает нашу фигуры (все её точки) на определенное рандомное значение point(x,y)
        // результат на экран не вывожу, так как в задании не стояло такого условия
        for (int i = 0; i < 10; i++) {
            randshapes[i].move(new Point((int) (Math.random() * 20), (int) (Math.random() * 20)));
        }
        // вызовем метод для изменение размера для каждой фигуры из массива
        // в методе умножаем полученную ранее сторону/радиус на рандомный коофициент от 0,1 до 1
        for (int i = 0; i < 10; i++) {
            randshapes[i].resize(Math.random() + 0.1);
        }
        System.out.println();
        System.out.println();
        System.out.println("Массив фигур с изменённым размером:");
        for (int i = 0; i < 10; i++) {
            System.out.println();
            System.out.print("Фигура " + randshapes[i].name);
            System.out.print("\t| Новые координаты первой точки: [" + randshapes[i].position.getX() + ", " + randshapes[i].position.getY() + "]");
            System.out.print("\t| Новая длина стороны/радиуса: ");
            System.out.printf("\t%11.2f", (randshapes[i].side));
        }
        System.out.println();
        System.out.println();
        // отсортируем фигуры по убыванию площади "методом пузырька"
        System.out.println("Массив фигур, отсортированный по возрастанию площади:");
        for (int i = 9; i >= 0; i--) {
            for (j = 0; j < i; j++) {
                if (randshapes[j].square() < randshapes[j + 1].square()) {
                    Shape tempShape = randshapes[j];
                    randshapes[j] = randshapes[j + 1];
                    randshapes[j + 1] = tempShape;
                }
            }
            System.out.println();
            System.out.print("Фигура " + randshapes[i].name);
            System.out.print("\t| Координата первой точки: [" + randshapes[i].position.getX() + ", " + randshapes[i].position.getY() + "]");
            System.out.print("\t| Длина стороны/радиуса: ");
            System.out.printf("\t%11.2f", (randshapes[i].side));
            System.out.print("\t| Площадь: ");
            System.out.printf("\t%11.2f", randshapes[j].square());
        }

    }
}




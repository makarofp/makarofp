package figur;

import java.awt.Point;

/**
 * Created by makarof on 01.12.2016.
 */

public abstract class Shape {
    public double[][] masCoor;
    public String name;
    public Point position;
    public double side;

    //Класс Shape содержит в себе несколько переменных: position - первая точка на системе координат,  side - сторона или радиус, name - название фигуры
    public Shape(Point position, double side, String name) {
        this.position = position;
        this.side = side;
        this.name = name;
    }

    public abstract void move(Point newposition);

    public void resize(double randomSize) {
    }

    public abstract double square();

}

package webdrivertest.amazone;


import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by makarof on 16.01.2017.
 */
public class TestAmazone {
    public static WebDriver driver;


    @Before
    public void beforeClass() {
        driver = new ChromeDriver();
        driver.get("https://www.amazon.com");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        System.out.println("title=" + driver.getTitle());
        Assert.assertTrue("Title не совпадает" + driver.getTitle(), driver.getTitle().equals("Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more"));

    }

    @After
    public void afterClass() {
        driver.quit();
    }

    /*
    Test 001 Search
    В поле поиска вводим ключевое слово: 'duck' и нажимаем значок поиска (лупу)
    Проверяем. что каждый из результатов содержит ключевое слово (в том числе в другом регистре)*/
    @Test
    public void testSearch001() {
        WebElement searchString = driver.findElement(By.cssSelector("#twotabsearchtextbox"));
        searchString.sendKeys("duck");
        WebElement searchButtom = driver.findElement(By.cssSelector((".nav-sprite .nav-input")));
        searchButtom.click();
        boolean checkDuck;
        ArrayList<WebElement> arrayOfItems = new ArrayList(driver.findElements(By.cssSelector(".s-access-title")));
        for (WebElement el : arrayOfItems) {
            checkDuck = el.getText().contains("duck") || el.getText().contains("DUCK") || el.getText().contains("Duck");
            Assert.assertTrue("В одном из товаров нет слова duck. Его название: " + el.getText(), checkDuck == true);
        }
    }

    /*
    Test 002 Search
    1. В поле поиска вводим ключевое слово: 'duck' и нажимаем значок поиска (лупу)
    2. Запоминаем количество найденных результатов.
    3. В блоке 'Shop by Category' кликаем на 'Baby Products'.
    4. На загруженной странице првоеряем, что результатов стало меньше для более целевой категории, чем было в общем поиске по одному слову.
    */
    @Test
    public void testSearch002() {
        WebElement searchString = driver.findElement(By.cssSelector("#twotabsearchtextbox"));
        searchString.sendKeys("duck");
        WebElement searchButtom = driver.findElement(By.cssSelector((".nav-sprite .nav-input")));
        searchButtom.click();
        WebElement quantityString = driver.findElement(By.cssSelector("#s-result-count"));
        String parseQuantityString = quantityString.getText();
        System.out.println(parseQuantityString);
        String[] arrayQuantityString = parseQuantityString.split(" ");
        String my = arrayQuantityString[2].replace(",", "");
        Integer i1 = Integer.parseInt(my);
        WebElement babyProducts = driver.findElement(By.cssSelector(".cell2 .acs-mn2-cellTitle"));
        babyProducts.click();
        WebElement sortQuantityString = driver.findElement(By.cssSelector("#s-result-count"));
        String parseSortQuantityString = sortQuantityString.getText();
        System.out.println(parseSortQuantityString);
        String[] arrayQuantityString1 = parseSortQuantityString.split(" ");
        String my1 = arrayQuantityString1[2].replace(",", "");
        Integer i2 = Integer.parseInt(my1);
        Assert.assertFalse("Результатов меньше не стало. Было: " + i1 + ". Стало: " + i2, i1 < i2);
    }

    /*Test 003 Adding products to Cart
  1. В поле поиска вводим ключевое слово: 'knife kitchen' и нажимаем значок поиска (лупу)
  2. В результатах поиска находим любой 8-ми дюймовый нож (8 Inch). Открываем страницу с его листингом(описанием).
  3. Добавляем в корзину (Add to Cart). Запоминаем название и цену.
  4. Производим поиск по ключевому слову 'duck'.
  5. Выбираем первый результат. Запоминаем название и цену. Добавляем в корзину.
  6. Переходим в корзину (справа вверху кликаем значок  "тележка").
  7. Проверяем, что в корзине только 2 наших товара, которые мы выбрали, и цена совпдает. Првоеряем, что общая сумма покупки равна сумме цен двух выбранных товаров.
  Примечание: товары для поиска можно взять любые. У одно из них должно быть особенное свойство, прописанное в названии (как, например, размер ножа в моем примере)*/
    @Test
    public void testAddingProductsToCart003() {
        WebElement searchString = driver.findElement(By.cssSelector("#twotabsearchtextbox"));
        searchString.sendKeys("knife kitchen");
        WebElement searchButtom = driver.findElement(By.cssSelector((".nav-sprite .nav-input")));
        searchButtom.click();
        WebElement inch8Result = driver.findElement(By.xpath(".//*[@id='s-results-list-atf']//li//div[contains(@class,'a-row a-spacing-mini')]//a[contains(@title,'8 Inch')]"));
        inch8Result.click();
        String firstElName = driver.findElement(By.cssSelector("#productTitle")).getText();
        System.out.println("Название первого товара: " + firstElName);
        WebElement addtocartbutton = driver.findElement(By.cssSelector("#add-to-cart-button"));
        addtocartbutton.click();
        Double firstElPrice = Double.valueOf(driver.findElement(By.cssSelector("#hlb-subcart .a-text-bold")).getText().replace("$", ""));
        System.out.println("Цена первого товара: " + firstElPrice);
        WebElement searchString1 = driver.findElement(By.cssSelector("#twotabsearchtextbox"));
        searchString1.sendKeys("duck");
        WebElement searchButtom1 = driver.findElement(By.cssSelector((".nav-sprite .nav-input")));
        searchButtom1.click();
        WebElement firstDuck = driver.findElement(By.cssSelector("#result_0 .s-access-title"));
        firstDuck.click();
        String secondElName = driver.findElement(By.cssSelector("#productTitle")).getText();
        System.out.println("Название второго товара: " + secondElName);
        Double secondElPrice = Double.valueOf(driver.findElement(By.cssSelector("#priceblock_saleprice")).getText().replace("$", ""));
        System.out.println("Цена второго товара: " + secondElPrice);
        WebElement addtocartbutton1 = driver.findElement(By.cssSelector("#add-to-cart-button"));
        addtocartbutton1.click();
        WebElement cartButtom = driver.findElement(By.cssSelector("#nav-cart"));
        cartButtom.click();
        boolean checkItem;
        ArrayList<WebElement> checkItems = new ArrayList(driver.findElements(By.xpath(".//*[@id='activeCartViewForm']//ul/li//span[contains(@class,'sc-product-title')]")));
        for (WebElement el1 : checkItems) {
            checkItem = el1.getText().equals(firstElName) || el1.getText().equals(secondElName);
            Assert.assertTrue("В корзине есть лишний товар. Его название: " + el1.getText(), checkItem == true);
        }
        Double totalSum = Double.valueOf(driver.findElement(By.cssSelector(".sc-price-sign")).getText().replace("$", ""));
        double expTotalSum = Math.round((firstElPrice + secondElPrice) * 100.0) / 100.0;
        System.out.println("Фактическая сумма: " + totalSum);
        System.out.println("Ожидаемая сумма: " + expTotalSum);
        Assert.assertTrue("Сумма товаров не совпадает. Текущая сумма равна: " + totalSum + ". А должна быть: " + expTotalSum, totalSum == expTotalSum);
    }

    /*
    Test 004 Deleting products from Cart
    1. Находим и добавляем в корзину 3 любых товара.
    2. Заходим в корзину и убеждаемся. что там именно наши выбранные товары (по названию).
    3. Удаляем второй товар из списка.
    4. Проверяем, что в корзине остались два других товара, а удаленный исчез со страницы.*/
    @Test
    public void testDeletingProductsFromCart004() {
        WebElement searchString = driver.findElement(By.cssSelector("#twotabsearchtextbox"));
        searchString.sendKeys("allarm");
        WebElement searchButtom = driver.findElement(By.cssSelector((".nav-sprite .nav-input")));
        searchButtom.click();
        WebElement item1 = driver.findElement(By.cssSelector("#result_2 .s-access-title"));
        item1.click();
        String firstElName = driver.findElement(By.cssSelector("#productTitle")).getText();
        System.out.println("Название первого товара: " + firstElName);
        WebElement addtocartbutton1 = driver.findElement(By.cssSelector("#add-to-cart-button"));
        addtocartbutton1.click();
        driver.navigate().back();
        driver.navigate().back();
        WebElement item2 = driver.findElement(By.cssSelector("#result_3 .s-access-title"));
        item2.click();
        String secondElName = driver.findElement(By.cssSelector("#productTitle")).getText();
        System.out.println("Название второго товара: " + secondElName);
        WebElement addtocartbutton2 = driver.findElement(By.cssSelector("#add-to-cart-button"));
        addtocartbutton2.click();
        driver.navigate().back();
        driver.navigate().back();
        WebElement item3 = driver.findElement(By.cssSelector("#result_4 .s-access-title"));
        item3.click();
        String thirdElName = driver.findElement(By.cssSelector("#productTitle")).getText();
        System.out.println("Название третьего товара: " + thirdElName);
        WebElement addtocartbutton3 = driver.findElement(By.cssSelector("#add-to-cart-button"));
        addtocartbutton3.click();
        driver.navigate().refresh();
        WebElement cartButtom = driver.findElement(By.cssSelector("#nav-cart"));
        cartButtom.click();
        boolean checkItem;
        ArrayList<WebElement> checkItems = new ArrayList(driver.findElements(By.xpath(".//*[@id='activeCartViewForm']//ul/li//span[contains(@class,'sc-product-title')]")));
        for (WebElement el1 : checkItems) {
            checkItem = el1.getText().equals(firstElName) || el1.getText().equals(secondElName) || el1.getText().equals(thirdElName);
            Assert.assertTrue("В корзине есть лишний товар. Его название: " + el1.getText(), checkItem == true);
        }
        String removeElName = driver.findElement(By.xpath(".//*[@id='activeCartViewForm']/div[2]/div[2]//ul/li/span/a/span")).getText();
        System.out.println("Название удаляемого товара: " + removeElName);
        WebElement deleteSecondElem = driver.findElement(By.cssSelector(".sc-list-item-border:nth-child(2) .sc-action-delete input"));
        deleteSecondElem.click();
        boolean checkItemNew;
        ArrayList<WebElement> checkItemsAfterRemoves = new ArrayList(driver.findElements(By.xpath(".//*[@id='activeCartViewForm']//ul/li//span[contains(@class,'sc-product-title')]")));
        for (WebElement el1 : checkItemsAfterRemoves) {
            checkItemNew = (el1.getText().equals(firstElName) || el1.getText().equals(secondElName) || el1.getText().equals(thirdElName)) && (el1.getText() != removeElName);
            Assert.assertTrue("Есть проблема с товаром. Его название: " + el1.getText(), checkItemNew == true);
        }
    }

    /*
     Test 005 Sign in
     1. Наводим курсор на "Account & List"
     2. В развернувшемся окне жмём "Sign in"
     3. Вводим логин: "yatestru@yandex.ru" и пароль "1234546qw"
     4. Жмём "Sign in"
     5. Проверяем, что над "Account & List" написано "Hello, Pavel"
     */
    @Test
    public void testSignIn005() throws InterruptedException {
        WebElement searchString = driver.findElement(By.cssSelector("#twotabsearchtextbox"));
        searchString.click();
        WebElement accountList = driver.findElement(By.cssSelector("#nav-link-accountList .nav-line-2"));
        Actions action = new Actions(driver);
        action.moveToElement(accountList).build().perform();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement signIn = driver.findElement(By.xpath(".//*[@id='nav-flyout-ya-signin']/a/span"));
        signIn.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement enterLogin = driver.findElement(By.cssSelector("#ap_email"));
        enterLogin.sendKeys("yatestru@yandex.ru");
        WebElement enterPass = driver.findElement(By.cssSelector("#ap_password"));
        enterPass.sendKeys("123456qw");
        WebElement clickSignIn = driver.findElement(By.cssSelector("#signInSubmit"));
        clickSignIn.click();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        WebElement helloPavel = driver.findElement(By.cssSelector("#nav-link-accountList .nav-line-1"));
        Assert.assertEquals("Вы не залогинились", "Hello, Pavel", helloPavel.getText());
    }
}





package webdrivertest.stackoverflow;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by makarof on 23.01.2017.
 */
public class TestStackoverflow {
    public static WebDriver driver;


    @Before
    public void beforeClass() {
        driver = new ChromeDriver();
        driver.get("http://stackoverflow.com/");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        System.out.println("title=" + driver.getTitle());
        Assert.assertTrue("Title не совпадает" + driver.getTitle(), driver.getTitle().equals("Stack Overflow"));

    }

    @After
    public void afterClass() {
        driver.quit();
    }

    /*
    Test 001
    Проверить, что количество в табе ‘featured’ главного блока сайта больше 300
     */
    @Test
    public void testBountyMore300() {
        WebElement featureCount = driver.findElement(By.cssSelector(".bounty-indicator-tab"));
        Integer featureCounts = new Integer(featureCount.getText());
        Assert.assertTrue("Ошибка. Вопросов с активным Баунти равно или меньше 300. Точнее их: " + featureCounts + "\n P.S. Спасибо яндексу за перевод фразы Question with an active bounty", featureCounts > 300);
    }

    /*Test 002
    Кликнуть на главной странице Sing Up, проверить что на открывшейся странице присутствуют кнопки для входа из социальных сетей - Google, Facebook*/
    @Test
    public void testCheckButtomForSocialRefistration() {
        WebElement signUpButtom = driver.findElement(By.xpath("html/body/div//span/a[contains(text(),'sign up')]"));
        signUpButtom.click();
        Assert.assertTrue("Нет кнопки Google", driver.findElement(By.cssSelector(".google-login .text")).isDisplayed());
        Assert.assertTrue("Нет кнопки Facebook", driver.findElement(By.cssSelector(".facebook-login .text")).isDisplayed());
    }

    /*Test 003
    Кликнуть на главной странице любой вопрос из секции Top Questions и на открывшейся странице проверить, что вопрос был задан сегодня.*/
    @Test
    public void testCheckIsTodayQuestion() {
        ArrayList<WebElement> arrayQuestion = new ArrayList(driver.findElements(By.xpath(".//*[@id='question-mini-list']//h3")));
        int x = (int) (Math.random() * 10);
        System.out.println("Для проверки откроем вопрос под номером " + (x + 1));
        arrayQuestion.get(x).click();
        WebElement dateQuestion = driver.findElement(By.xpath(".//*[@id='qinfo']/tbody/tr[1]//b"));
        Assert.assertTrue("Вопрос был задан не сегодня, а " + dateQuestion.getText(), dateQuestion.getText().contains("today") == true);
    }

    /*
    Test 004
    Проверить, что на главной странице stackoverflow есть предложение о работе с зарплатой больше $ 60k :) (тест должен упасть, если такого предложения нет).*/
    @Test
    public void testCkeckSalaryMore60K() {
        ArrayList<WebElement> salary = new ArrayList(driver.findElements(By.cssSelector(".salary")));
        for (WebElement elSalary : salary) {
            Double chislo = 0.0;
            String k = elSalary.getText().toString().replace(",", "");
            if (k.contains("-")) {
                String[] salaryParts = k.split("-");
                chislo = Double.valueOf(salaryParts[1].substring(2));
            } else {
                chislo = Double.valueOf(k.substring(0));
            }
            if (k.contains("£")) {
                chislo *= 1.25;
            } else {
            }
            if (k.contains("€")) {
                chislo *= 1.07;
            } else {
            }
            System.out.println(chislo);
            Assert.assertTrue("Не хотят платить нормальные зарплаты. Платят только " + chislo, chislo > 60000);
        }
    }


    /*Test 005
    В футере кликнуть на пункт меню about us.
    Проверяем, что урл страницы, на которую переходим, правильный.
    */
    @Test
    public void testCheckLinkAbout() {
        WebElement aboutUsButtom = driver.findElement(By.cssSelector(".top-footer-links > a:nth-child(1)"));
        aboutUsButtom.click();
        Assert.assertEquals("Урл не корректный. Фактический: " + driver.getCurrentUrl() + " А ожидаемый: http://stackoverflow.com/company/about", driver.getCurrentUrl(), "http://stackoverflow.com/company/about");
    }
}

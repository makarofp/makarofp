package Test.TestFigur;

import figur.Circle;
import org.junit.*;

import java.awt.*;

/**
 * Created by makarof on 21.12.2016.
 */
public class TestCircle {

    public static Circle circle;
    private static Point test_position = new Point(0, 0);
    private static double test_side = 2D;
    private static String test_name = "Круг";

    //Создаем экземпляр круга
    @BeforeClass
    public static void createCircle() {
        circle = new Circle(test_position, test_side, test_name);
    }

    //Инициализируем фигуру
    @Before
    public void initializeCircle() {
        circle.position = test_position;
        circle.side = test_side;
        circle.name = test_name;
    }

    //Бесполезный метод для того, что бы посмотреть на работу AfterClass
    @AfterClass
    public static void finalMessage() {
        System.out.println("Все тесты как-то прошли");
    }

    //проверяем что фигура не null
    @Test
    public void testCircleNotNull() {
        Assert.assertNotNull("Circle не должен быть null", circle);
    }

    //тест на перемещение круга
    @Test
    public void testMove() {
        circle.move(new Point(2, 5));
        Assert.assertTrue("Метод move работает некорректно. Ожидаемая точка после перемещения: [2.0,5.0], фактическая точка: [" + circle.position.getX() + "," + circle.position.getY() + "]", (circle.position.getX() == 2.0D & circle.position.getY() == 5.0D));
    }

    //тест на изменение размера круга
    @Test
    public void testResize() {
        circle.resize(5.1D);
        Assert.assertFalse("Метод resize работает некорректно. Ожидаемая длина стороны: 10.2, фактическая длина: " + circle.side, circle.side != 10.2D);
    }

    //тест на вычесление площади круга
    @Test
    public void testSquare() {
        double square = circle.square();
        Assert.assertEquals("Метод square работает некорректно. Ожидаемая площадь: 12.57, фактическая площадь: " + String.format("%11.2f", square), 12.57, square, 0.2);
    }

}

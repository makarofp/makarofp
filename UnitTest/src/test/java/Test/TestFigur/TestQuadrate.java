package Test.TestFigur;

import figur.Quadrate;
import org.junit.*;

import java.awt.*;

/**
 * Created by makarof on 21.12.2016.
 */
public class TestQuadrate {
    public static Quadrate quadrate;
    private static Point test_position = new Point (1,1);
    private static double test_side=8D;
    private static String test_name = "Квадрат";

    //Создаем экземпляр круга
    @BeforeClass
    public static void createCircle(){
        quadrate = new Quadrate(test_position, test_side, test_name);
    }
    //Инициализируем фигуру
    @Before
    public void initializeCircle(){
        quadrate.position = test_position;
        quadrate.side = test_side;
        quadrate.name = test_name;
    }

    //Бесполезный метод для того, что бы посмотреть на работу After
    @After
    public void finalMessage(){
        System.out.println("Тест завершен");

    }

    //проверяем, что фигура не null
    @Test
    public void testCircleNotNull() {
        Assert.assertNotNull("Circle не должен быть null", quadrate);
    }

    //тест на перемещение квадрата по оси X
    @Test
    public void testMove(){
        quadrate.move(new Point (6,0));
        Assert.assertTrue("Метод move работает некорректно. Ожидаемая точка после перемещения: [7.0,1.0], фактическая точка: [" + quadrate.position.getX() + "," + quadrate.position.getY() + "]", (quadrate.position.getX()==7.0D & quadrate.position.getY()==1.0D));
    }

    //тест на изменение размера квадрата
    @Test
    public void testResize(){
        quadrate.resize(0.5D);
        Assert.assertFalse("Метод resize работает некорректно. Ожидаемая длина стороны: 4, фактическая длина: " + quadrate.side, quadrate.side!=4D);
    }

    //тест на вычесление площади квадрата
    @Test
    public void testSquare(){
        double square = quadrate.square();
        Assert.assertEquals("Метод square работает некорректно. Ожидаемая площадь: 64,00, фактическая площадь: " + String.format("%11.2f",square), 64, square, 0.2);
    }
}

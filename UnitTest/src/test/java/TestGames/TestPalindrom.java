package TestGames;

import games.Palindrom;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by makarof on 04.01.2017.
 */
public class TestPalindrom {
    //тест проверяет, что слова палндромы определяются как палиндромом
    @Test
    public void testWordIsPalindrom() {
        String exp1 = "шалаш";
        boolean act1 = Palindrom.checkWord(exp1);
        Assert.assertTrue("Я вводил полиндром: " + exp1, act1);
    }

    //тест проверяет, что слова не палндромы не определяются как палиндромом
    @Test
    public void testWordIsNotPalindrom() {
        String exp2 = "калаш";
        boolean act2 = Palindrom.checkWord(exp2);
        Assert.assertFalse("Я вводил не полиндром: " + exp2, act2);
    }

    //тест проверяет, что слова слова с пробелами не определяются как палиндромы
    @Test
    public void testWordWithSpace() {
        String exp3 = "le veL";
        boolean act3 = Palindrom.checkWord(exp3);
        Assert.assertFalse("Я вводил не полиндром: " + exp3, act3);
    }

    //тест проверяет, что фразы палндромы определяются как палиндромы
    @Test
    public void testPhraseIsPalindrom() {
        String exp4 = "а картина манит рака";
        boolean act4 = Palindrom.checkPhrase(exp4);
        Assert.assertTrue("Я вводил полиндром: " + exp4, act4);
    }

    //тест проверяет, что фразы не палндромы не определяются как палиндромомы
    @Test
    public void testPhraseIsNotPalindrom() {
        String exp5 = "it is my live";
        boolean act5 = Palindrom.checkPhrase(exp5);
        Assert.assertFalse("Я вводил не полиндром: " + exp5, act5);
    }

    ////тест проверяет, что заглавные буквы не влияют на определения палидромизма
    @Test
    public void testPhraseToLowerReg() {
        String exp6 = "А Лида гадила";
        boolean act6 = Palindrom.checkPhrase(exp6);
        Assert.assertTrue("Я вводил полиндром: " + exp6, act6);
    }
}

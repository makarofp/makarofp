package TestGames;

import games.Matrix;
import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;

/**
 * Created by makarof on 22.12.2016.
 */
public class TestMatrix {

    //тест на проверку того, что матрица не null
    @Test
    public void testMatrixNotNull() {

        Assert.assertNotNull("Матрица не матрица, а null", Matrix.getmatrix(4));
    }

    //проверяем, что матрица создается с заданной размерностью
    @Test
    public void testMatrixRazmer() {
        int[][] testMatrixRazm = Matrix.getmatrix(3);
        int[][] expectMatrix = {
                {1, 2, 3},
                {4, 5, 6,},
                {7, 8, 9}
        };
        Assert.assertTrue("Метод создания матрицы заданного размера не работает. Ожидаемый результат: " + Arrays.deepToString(expectMatrix) + "Фактический результат: " + Arrays.deepToString(testMatrixRazm), Arrays.deepToString(testMatrixRazm).equals(Arrays.deepToString(expectMatrix)));
    }

    //тест на проверку того, что в матрице все элементы меньше или равны 9
    @Test
    public void testFindElementMoreNine() {

        int[][] MoreNineMatrix = Matrix.getmatrix(3);

        int k = 0;
        int str = 0;
        int kol = 0;
        for (int i = 0; i < MoreNineMatrix.length; i++) {
            for (int j = 0; j < MoreNineMatrix[i].length; j++) {
                if (MoreNineMatrix[i][j] > 9) {
                    k = MoreNineMatrix[i][j];
                    str = i;
                    kol = j;
                    break;
                }
            }
        }
        Assert.assertFalse("В массиве есть числа больше 9. Это число " + k + "порядковый номер эллемента: [" + str + "] [" + kol + "]", k != 0);
    }
}
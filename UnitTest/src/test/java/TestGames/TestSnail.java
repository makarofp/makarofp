package TestGames;

import games.Snail;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by makarof on 23.12.2016.
 */
public class TestSnail {

    //тест на проверку того, что самое большое число массива равно размерности массива в квадрате
    @Test
    public void testLastElementRavenRazmerVkvadrate() {
        int n = 15;
        int max_el = 0;
        int[][] testCalculateSnail = Snail.calculateSnail(n);
        int k = (int) Math.pow(n, 2);

        for (int i = 0; i < testCalculateSnail.length; i++) {
            for (int j = 0; j < testCalculateSnail[i].length; j++) {

                if (testCalculateSnail[i][j] > max_el) {
                    max_el = testCalculateSnail[i][j];
                }
            }
        }
        Assert.assertEquals("Что-то пошло не так. Самое большое число массива: " + max_el + ", а размернность массива в квадрате: " + k, k, max_el);
    }

    //тест проверяет, что сумма всех чисел массива равна заданной. Это коственно означает, что массив заполнен корректными значениями
    @Test
    public void testSumAllElem() {
        int n = 3;
        int[][] arrSumAllElem = Snail.calculateSnail(n);
        int sum = 0;
        for (int i = 0; i < arrSumAllElem.length; i++) {
            for (int j = 0; j < arrSumAllElem[i].length; j++) {
                sum += arrSumAllElem[i][j];
            }
        }
        Assert.assertTrue("Массив заполнен не коректно. Ожидаемый результат: 45 а фактически сумма всех элементов равна" + sum, sum == 45);
    }

    //тест проверяет, что минимальный элемент в массиве равен 1
    @Test
    public void testMinElem() {
        int n = 4;
        int[][] arrMinElem = Snail.calculateSnail(n);
        int min_el = 1;
        for (int i = 0; i < arrMinElem.length; i++) {
            for (int j = 0; j < arrMinElem[i].length; j++) {
                min_el = Math.min(min_el, arrMinElem[i][j]);
            }
        }
        Assert.assertEquals("Минимальный элемент не 1, а " + min_el, min_el, 1);
    }

    //тест проверяет, что в массиве нет null эдементов
    @Test
    public void testNotNullElem() {
        int n = 5;
        int[][] arrNotNullElem = Snail.calculateSnail(n);
        for (int i = 0; i < arrNotNullElem.length; i++) {
            for (int j = 0; j < arrNotNullElem[i].length; j++) {
                Assert.assertNotNull("Один из элементов null", arrNotNullElem[i][j]);
            }
        }
    }
}


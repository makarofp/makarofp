package figur;

import java.awt.*;

/**
 * Created by makarof on 01.12.2016.
 */

public class Circle extends Shape {


    //Круг описываем центральной точкой (position) и радиусом (side)
    public Circle(Point position, double side, String name) {
        super(position, side, name);
        double[][] masCoor = new double[1][2];
        masCoor[0][0] = position.getX();
        masCoor[0][1] = position.getY();

    }

    //Смещаем все точки на значение newposition
    //Х-координату сместим на newposition.X
    //Y-координату сместим на newposition.Y
    @Override
    public void move(Point newposition) {
        position.setLocation(position.getX() + newposition.getX(), position.getY() + newposition.getY());
    }

    @Override
    public void resize(double randomSize) {
        side *= randomSize;
    }

    //Площадь круга = Pi*r*r, где Pi - 3.14, а r - радиус круга
    @Override
    public double square() {
        return Math.PI * Math.pow(side, 2);
    }
}





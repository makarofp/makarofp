package figur;

import java.awt.*;

/**
 * Created by makarof on 01.12.2016.
 */
public class Quadrate extends Shape {


    //Квадрат описываем одной точкой (position) и длинной стороны (side).
    //Примим за допущение, что эта точка - это всегда левая нижняя точка нашего квадрата.
    //Точка 2 будет смещена по оси Х на side, точка 3 будет смещена по оси Y на side.
    //Точка 4 - по обоим осям на side
    public Quadrate(Point position, double side, String name) {
        super(position, side, name);
        double[][] masCoor = new double[4][2];
        masCoor[0][0] = position.getX();
        masCoor[0][1] = position.getY();
        masCoor[1][0] = position.getX() + side;
        masCoor[1][1] = position.getY();
        masCoor[2][0] = position.getX();
        masCoor[2][1] = position.getY() + side;
        masCoor[3][0] = position.getX() + side;
        masCoor[3][1] = position.getY() + side;
    }

    //Смещаем все точки на значение newposition
    //все Х-координаты сместим на newposition.X
    //все Y-координаты сместим на newposition.Y
    @Override
    public void move(Point newposition) {
        position.setLocation(position.getX() + newposition.getX(), position.getY() + newposition.getY());
    }

    @Override

    public void resize(double randomSize) {
        side *= randomSize;
    }

    //Площадь квадрата = а*а, где а - сторона квадрата
    @Override
    public double square() {
        return Math.pow(side, 2);
    }
}
